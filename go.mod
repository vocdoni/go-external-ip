module gitlab.com/vocdoni/go-external-ip

go 1.12

require (
	github.com/google/pprof v0.0.0-20190908185732-236ed259b199 // indirect
	github.com/ianlancetaylor/demangle v0.0.0-20181102032728-5e5cf60278f6 // indirect
	golang.org/x/arch v0.0.0-20190919213554-7fe50f7625bd // indirect
	golang.org/x/crypto v0.0.0-20190911031432-227b76d455e7 // indirect
	golang.org/x/sys v0.0.0-20190919044723-0c1ff786ef13 // indirect
	golang.org/x/tools v0.0.0-20190919223014-db1d4edb4685 // indirect
)
